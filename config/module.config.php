<?php
namespace paml\SessionTranslator;

return [
    'controller_plugins' => [
        'factories' => [
            Plugin\Translate::class => Factory\TranslateFactory::class,
        ],
        'aliases' => [
            'translate' => Plugin\Translate::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Listener\LanguageListener::class => Factory\LanguageListenerFactory::class,
            Service\TranslatorService::class => Factory\TranslatorServiceFactory::class,
        ],
    ],
    'session_containers' => [
        'Route\Language'
    ],
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'phparray',
                'base_dir' => './data/languages',
                'pattern'  => '%s.php',
            ],
        ],
    ],
    'view_helpers' => [
        'factories' => [
            Plugin\Translate::class => Factory\TranslateFactory::class,
            Helper\Language::class => Factory\LanguageFactory::class,
        ],
        'aliases' => [
            'translate' => Plugin\Translate::class,
            'language' => Helper\Language::class,
        ],
    ],
];
