<?php

namespace paml\SessionTranslator\Factory;

use Interop\Container\ContainerInterface;
use paml\SessionTranslator\Helper\Language;
use Zend\ServiceManager\Factory\FactoryInterface;

class LanguageFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new Language(
            $container->get('Config')['languages_list'],
            $container->get('Route\Language')
        );
    }
}
