<?php

namespace paml\SessionTranslator\Factory;

use Interop\Container\ContainerInterface;
use paml\SessionTranslator\Listener\LanguageListener;
use Zend\ServiceManager\Factory\FactoryInterface;

class LanguageListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new LanguageListener(
            $container->get('Route\Language')
        );
    }
}
