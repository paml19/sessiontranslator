<?php

namespace paml\SessionTranslator\Factory;

use Interop\Container\ContainerInterface;
use paml\SessionTranslator\Plugin\Translate;
use paml\SessionTranslator\Service\TranslatorService;
use Zend\ServiceManager\Factory\FactoryInterface;

class TranslateFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new Translate(
            $container->get(TranslatorService::class)
        );
    }
}
