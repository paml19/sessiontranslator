<?php

namespace paml\SessionTranslator\Factory;

use Interop\Container\ContainerInterface;
use paml\SessionTranslator\Service\TranslatorService;
use Zend\I18n\Translator\Translator;
use Zend\ServiceManager\Factory\FactoryInterface;

class TranslatorServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $locale = ($container->get('Route\Language'))->locale;

        $translator = new Translator();
        $translator->setLocale($locale);

        return new TranslatorService($translator);
    }
}
