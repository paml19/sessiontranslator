<?php

namespace paml\SessionTranslator\Helper;

use Zend\Session\Container;
use Zend\View\Helper\AbstractHelper;

class Language extends AbstractHelper
{
    private $languages;

    private $container;

    public function __construct(array $languages, Container $container)
    {
        $this->languages = $languages;
        $this->container = $container;
    }

    public function getList(): array
    {
        return $this->languages;
    }

    public function getCurrent(): string
    {
        return $this->container->locale;
    }
}
