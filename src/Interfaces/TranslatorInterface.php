<?php

namespace paml\SessionTranslator\Interfaces;

use paml\SessionTranslator\Service\TranslatorService;

interface TranslatorInterface
{
    public function setTranslator(TranslatorService $translator): void;

    public function translate(string $value): string;
}
