<?php

namespace paml\SessionTranslator\Listener;

use paml\SessionTranslator\Module;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\Controller\AbstractController;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\Console\Request as ConsoleRequest;

class LanguageListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $sharedEventManager = $events->getSharedManager();

        $sharedEventManager->attach(
            AbstractController::class,
            MvcEvent::EVENT_DISPATCH,
            [
                $this,
                'onDispatch',
            ],
            $priority
        );

        $sharedEventManager->attach(
            AbstractController::class,
            Module::CHANGE_LANGUAGE,
            [
                $this,
                'onChangeLanguage',
            ],
            $priority
        );
    }

    public function onDispatch(MvcEvent $mvcEvent)
    {
        if ($mvcEvent->getRequest() instanceof ConsoleRequest) {
            return true;
        }

        if (! isset($this->container->locale)) {
            $this->container->locale = current(
                $mvcEvent
                    ->getRequest()
                    ->getHeader('Accept-Language')
                    ->getPrioritized()
            )
                ->getLanguage();
        }
    }

    public function onChangeLanguage(EventInterface $event)
    {
        $language = $event->getParam('language');
        $this->container->locale = $language;
    }
}
