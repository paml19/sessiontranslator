<?php

namespace paml\SessionTranslator;

use paml\SessionTranslator\Listener\LanguageListener;
use Zend\Mvc\MvcEvent;

class Module
{
    const CHANGE_LANGUAGE = 'changeLanguage';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        $serviceManager = $event->getApplication()->getServiceManager();
        $eventManager = $event->getTarget()->getEventManager();

        $languageListener = $serviceManager->get(LanguageListener::class);
        $languageListener->attach($eventManager, 1);
    }
}
