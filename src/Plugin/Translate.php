<?php

namespace paml\SessionTranslator\Plugin;

use paml\SessionTranslator\Service\TranslatorService;

class Translate extends \Zend\I18n\View\Helper\Translate
{
    private $translatorService;

    public function __construct(TranslatorService $translatorService)
    {
        $this->translatorService = $translatorService;
    }

    public function __invoke($message, $textDomain = null, $locale = null)
    {
        return parent::__invoke($message, $textDomain, $this->translatorService->getTranslator()->getLocale());
    }
}
