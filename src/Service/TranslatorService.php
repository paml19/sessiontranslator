<?php

namespace paml\SessionTranslator\Service;

use Zend\I18n\Translator\Translator;

class TranslatorService
{
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function getTranslator(): Translator
    {
        return $this->translator;
    }
}
