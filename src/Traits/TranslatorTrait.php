<?php

namespace paml\SessionTranslator\Traits;

use paml\SessionTranslator\Service\TranslatorService;

trait TranslatorTrait
{
    /** @var TranslatorService */
    private $translator;

    public function setTranslator(TranslatorService $translator): void
    {
        $this->translator = $translator;
    }

    public function translate(string $value): string
    {
        return $this->translator->getTranslator()->translate($value);
    }

    public function getLocale(): string
    {
        $this->translator->getTranslator()->getLocale();
    }
}
